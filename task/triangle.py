def is_triangle(a, b, c):
    '''
    please add your solution here or call your solution implemented in different function from here  
    then change return value from 'False' to value that will be returned by your solution
    '''

    if (a <= 0 or b <= 0 or c <= 0):
        return False
    else:
        if(a + b > c and b + c > a and a + c > b):
            return True
        else:
            return False

